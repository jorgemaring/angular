import {v4 as uuid} from 'uuid';
export class DestinoViaje {
    selected: boolean;
    servicios: string[];
    id = uuid();

    constructor(
        public nombre: string,
        public u: string,
        public votes: number = 0
    ) {
        this.servicios = ['desayuno', 'gimnasio'];
    }
    isSelected(): boolean {
        return this.selected;
    }
    setSelected(b: boolean) {
        this.selected = b;
    }
    voteUp() {
        this.votes++;
    }
    voteDown() {
        this.votes--;
    }
  }
