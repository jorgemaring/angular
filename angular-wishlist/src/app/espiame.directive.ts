import { Directive , OnInit, OnDestroy} from '@angular/core';

@Directive({
  selector: '[appEspiame]'
})
export class EspiameDirective implements OnInit, OnDestroy {
  static nextId = 0;
  log = (msg: string) => console.log(`evento#${EspiameDirective.nextId++}${msg}`);
  ngOnInit() {this.log('######*****OnInit'); }

  ngOnDestroy() {this.log('######*****OnDestroy'); }
}
